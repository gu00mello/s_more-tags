/* If you're feeling fancy you can add interactivity 
    to your site with Javascript */

// prints "hi" in the browser's dev tools console
console.log("hi");

$('.change-theme').click(function() {
    if ($(this).is(':checked') === true) {
        $('.teste').attr('href', 'black-style.css');
    } else {
        $('.teste').attr('href', 'style.css');
    }
})